package vlad;

import org.junit.Assert;
import org.junit.Test;
import vlad.Humans.Human;
import vlad.Humans.Man;

public class HumanTest {

    private Man human = new Man("Vlad", "Lietun", 1995, 100, null);
    private Man human1 = new Man("Denis", "Lietun", 1995, 100, null);

    public String getExpectedToString(Human human) {
        String result = "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", schedule=" + human.getSchedule() + "}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertEquals(human.toString(), getExpectedToString(human));
    }
}